export const isBlank = (valor) => {
    return valor === undefined || valor === null || valor === "null" || valor === "" || valor.length === 0;
}

export const isNotBlank = (valor) => {
    return !isBlank(valor);
}

export const formataCpfCnpj = (valor) => {
    return formatar_cnpj_cpf(valor);
}

function formatar_cnpj_cpf(vlr) {
    vlr = vlr.replace(".", "").replace(".", "").replace(".", "").replace("-", "").replace("/", "");

    if (vlr.length == 11) {
        return formatar_cpf(vlr);
    } else {
        return formatar_cnpj(vlr);
    }
}

function formatar_cpf(v) {
    v = v.replace(/\D/g, ""); //Remove tudo o que não é dígito
    v = v.replace(/(\d{3})(\d)/, "$1.$2"); //Coloca um ponto entre o terceiro e o quarto dígitos
    v = v.replace(/(\d{3})(\d)/, "$1.$2"); //Coloca um ponto entre o terceiro e o quarto dígitos
    v = v.replace(/(\d{3})(\d{1,2})$/, "$1-$2"); //Coloca um hífen entre o terceiro e o quarto dígitos
    return v;
}

function formatar_cnpj(v) {
    v = v.replace(/\D/g, ""); //Remove tudo o que não é dígito
    v = v.replace(/^(\d{2})(\d)/, "$1.$2"); //Coloca ponto entre o segundo e o terceiro dígitos
    v = v.replace(/^(\d{2})\.(\d{3})(\d)/, "$1.$2.$3"); //Coloca ponto entre o quinto e o sexto dígitos
    v = v.replace(/\.(\d{3})(\d)/, ".$1/$2"); //Coloca uma barra entre o oitavo e o nono dígitos
    v = v.replace(/(\d{4})(\d)/, "$1-$2"); //Coloca um hífen depois do bloco de quatro dígitos
    return v;
}