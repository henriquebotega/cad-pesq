import axios from 'axios'

export const ioURL = window.location.href.indexOf('localhost') > -1 ? 'http://localhost:4000' : 'https://socket-cadastros.herokuapp.com';

export const api = axios.create({
    baseURL: ioURL + '/api'
})