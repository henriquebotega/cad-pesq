import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Add, Create, Delete, InsertChartOutlined } from '@material-ui/icons';

import Actions from '../../../store/actions'

import './styles.css'

class PesqEmpr extends Component {

    componentDidMount() {
        const idPesquisa = this.props.match.params.idPesquisa;

        this.props.loadData(idPesquisa);
        this.props.loadPesquisaByID(idPesquisa);
    }

    handleNovo = () => {
        const idPesquisa = this.props.match.params.idPesquisa;
        this.props.history.push('/restrito/pesqempr/' + idPesquisa + '/novo/')
    }

    handleEditar = (i) => {
        const idPesquisa = this.props.match.params.idPesquisa;
        this.props.history.push('/restrito/pesqempr/' + idPesquisa + '/editar/' + i._id)
    }

    handleExcluir = (i) => {
        this.props.delByID(i._id)
    }

    handleGrafico = (i) => {

    }

    render() {
        return (
            <div id="box-container">
                <header>
                    <h1>Pesquisa por Empresa</h1>
                </header>

                <h3>{this.props.pesquisa.registro.titulo}</h3>

                <button onClick={() => this.handleNovo()}>
                    <Add style={{ color: '#2A9D8F', fontSize: 48 }} />
                </button>

                {this.props.pesqempr.isLoading &&
                    <div>Carregando...</div>
                }

                <ul>
                    {this.props.pesqempr.registros && this.props.pesqempr.registros.map((item, i) => (
                        <li key={i}>
                            {item._empresa.nome}

                            <span>
                                <button onClick={() => this.handleGrafico(item)}>
                                    <InsertChartOutlined style={{ fontSize: 24 }} />
                                </button>

                                <button onClick={() => this.handleEditar(item)}>
                                    <Create style={{ fontSize: 24 }} />
                                </button>

                                <button onClick={() => this.handleExcluir(item)}>
                                    <Delete style={{ fontSize: 24 }} />
                                </button>
                            </span>
                        </li>
                    ))}
                </ul>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    pesquisa: state.pesquisa,
    pesqempr: state.pesqempr
})

const mapDispatchToProps = dispach => ({
    loadPesquisaByID: (idPesquisa) => dispach(Actions.editPesquisaRequest(idPesquisa)),
    loadData: (idPesquisa) => dispach(Actions.getPesqEmprRequest(idPesquisa)),
    delByID: (id) => dispach(Actions.delPesqEmprRequest(id)),
})

export default connect(mapStateToProps, mapDispatchToProps)(PesqEmpr)