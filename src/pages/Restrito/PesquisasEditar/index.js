import React, { Component } from 'react'
import { connect } from 'react-redux'
import Select from 'react-select'

import Actions from '../../../store/actions'
import { Save } from '@material-ui/icons';

class PesquisasEditar extends Component {

    state = {
        registro: {
            titulo: '',
            _categoria: null,
        },
        colCategorias: []
    }

    componentDidMount() {
        const id = this.props.match.params.id;
        this.props.loadByID(id)
        this.props.loadCategorias();
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.pesquisa && nextProps.pesquisa.registro && nextProps.pesquisa.registro.titulo) {
            this.setState({
                registro: {
                    ...nextProps.pesquisa.registro
                }
            });
        }

        if (nextProps.categoria && nextProps.categoria.registros && nextProps.categoria.registros.length > 0) {
            this.setState({
                colCategorias: nextProps.categoria.registros.map((regAtual) => { return { value: regAtual._id, label: regAtual.titulo } })
            });
        }
    }

    handleSalvar = async (e) => {
        e.preventDefault();

        const id = this.props.match.params.id;
        this.props.save(id, this.state.registro)

        this.props.history.push('/restrito/pesquisas')
    }

    handleInputChange = (e) => {
        const { registro } = { ...this.state };
        const currentState = registro;
        const { name, value } = e.target;
        currentState[name] = value;

        this.setState({
            registro: currentState
        })
    }

    handleSelectChange = (e) => {
        this.setState({
            registro: {
                ...this.state.registro,
                _categoria: e.value
            }
        })
    }

    selectOptionValue = () => {
        if (this.state.registro._categoria) {
            const idCategoria = ((this.state.registro._categoria._id) ? this.state.registro._categoria._id : this.state.registro._categoria)
            return this.state.colCategorias.filter((regAtual) => regAtual.value === idCategoria)
        }
    }

    render() {
        return (
            <div id="box-container">
                <header>
                    <h1>Editar Pesquisa</h1>
                </header>

                {this.props.pesquisa.isSaving &&
                    <div>Carregando...</div>
                }

                {!this.props.pesquisa.isSaving &&
                    <form onSubmit={(e) => this.handleSalvar(e)}>
                        <p>
                            Titulo:
                            <input type="text" name="titulo" value={this.state.registro.titulo} onChange={this.handleInputChange} />
                        </p>

                        <div>
                            Categoria:
                            <Select value={this.selectOptionValue()} onChange={this.handleSelectChange} options={this.state.colCategorias} />
                        </div>

                        <button>
                            <Save style={{ color: '#2A9D8F', fontSize: 48 }} />
                        </button>
                    </form>
                }

            </div>
        )
    }
}

const mapStateToProps = state => ({
    categoria: state.categoria,
    pesquisa: state.pesquisa
})

const mapDispatchToProps = dispach => ({
    loadCategorias: () => dispach(Actions.getCategoriasRequest()),
    loadByID: (id) => dispach(Actions.editPesquisaRequest(id)),
    save: (id, registro) => dispach(Actions.savePesquisaRequest(id, registro))
})

export default connect(mapStateToProps, mapDispatchToProps)(PesquisasEditar)