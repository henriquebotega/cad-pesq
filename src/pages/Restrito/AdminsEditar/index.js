import React, { Component } from 'react'
import { connect } from 'react-redux'
import Select from 'react-select'

import Actions from '../../../store/actions'
import { Save } from '@material-ui/icons';

class AdminsEditar extends Component {

    state = {
        registro: {
            login: '',
            password: '',
            email: '',
            _empresa: null,
        },
        colEmpresas: []
    }

    componentDidMount() {
        const id = this.props.match.params.id;
        this.props.loadByID(id)
        this.props.loadEmpresas()
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.admin && nextProps.admin.registro && nextProps.admin.registro.login) {
            this.setState({
                registro: {
                    ...nextProps.admin.registro
                }
            });
        }

        if (nextProps.empresa && nextProps.empresa.registros && nextProps.empresa.registros.length > 0) {
            this.setState({
                colEmpresas: nextProps.empresa.registros.map((regAtual) => { return { value: regAtual._id, label: regAtual.nome } })
            });
        }
    }

    handleSalvar = async (e) => {
        e.preventDefault();

        const id = this.props.match.params.id;
        this.props.save(id, this.state.registro)

        this.props.history.push('/restrito/admins')
    }

    handleInputChange = (e) => {
        const { registro } = { ...this.state };
        const currentState = registro;
        const { name, value } = e.target;
        currentState[name] = value;

        this.setState({
            registro: currentState
        })
    }

    handleSelectChange = (e) => {
        this.setState({
            registro: {
                ...this.state.registro,
                _empresa: e.value
            }
        })
    }

    selectOptionValue = () => {
        if (this.state.registro._empresa) {
            const idEmpresa = ((this.state.registro._empresa._id) ? this.state.registro._empresa._id : this.state.registro._empresa)
            return this.state.colEmpresas.filter((regAtual) => regAtual.value === idEmpresa)
        }
    }

    render() {
        return (
            <div id="box-container">
                <header>
                    <h1>Editar Admin</h1>
                </header>

                {this.props.admin.isSaving &&
                    <div>Carregando...</div>
                }

                {!this.props.admin.isSaving &&
                    <form onSubmit={(e) => this.handleSalvar(e)}>
                        <p>
                            Login:
                            <input type="text" name="login" value={this.state.registro.login} onChange={this.handleInputChange} />
                        </p>

                        <p>
                            Pasword:
                            <input type="password" name="password" value={this.state.registro.password} onChange={this.handleInputChange} />
                        </p>

                        <p>
                            E-mail
                            <input type="text" name="email" value={this.state.registro.email} onChange={this.handleInputChange} />
                        </p>

                        <div>
                            Empresa:
                            <Select value={this.selectOptionValue()} onChange={this.handleSelectChange} options={this.state.colEmpresas} />
                        </div>

                        <button>
                            <Save style={{ color: '#2A9D8F', fontSize: 48 }} />
                        </button>
                    </form>
                }

            </div>
        )
    }
}

const mapStateToProps = state => ({
    empresa: state.empresa,
    admin: state.admin
})

const mapDispatchToProps = dispach => ({
    loadEmpresas: () => dispach(Actions.getEmpresasRequest()),
    loadByID: (id) => dispach(Actions.editAdminRequest(id)),
    save: (id, registro) => dispach(Actions.saveAdminRequest(id, registro))
})

export default connect(mapStateToProps, mapDispatchToProps)(AdminsEditar)