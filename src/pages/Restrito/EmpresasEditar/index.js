import React, { Component } from 'react'
import { connect } from 'react-redux'

import Actions from '../../../store/actions'
import { Save } from '@material-ui/icons';

class EmpresasEditar extends Component {

    state = {
        registro: {
            nome: '',
            razaoSocial: '',
            cpfCnpj: '',
            email: ''
        }
    }

    componentDidMount() {
        const id = this.props.match.params.id;
        this.props.loadByID(id)
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.empresa && nextProps.empresa.registro && nextProps.empresa.registro.nome) {
            this.setState({
                registro: {
                    ...nextProps.empresa.registro
                }
            });
        }
    }

    handleSalvar = async (e) => {
        e.preventDefault();

        const id = this.props.match.params.id;
        this.props.save(id, this.state.registro)

        this.props.history.push('/restrito/empresas')
    }

    handleInputChange = (e) => {
        const { registro } = { ...this.state };
        const currentState = registro;
        const { name, value } = e.target;
        currentState[name] = value;

        this.setState({
            registro: currentState
        })
    }

    render() {
        return (
            <div id="box-container">
                <header>
                    <h1>Editar Empresa</h1>
                </header>

                {this.props.empresa.isSaving &&
                    <div>Carregando...</div>
                }

                {!this.props.empresa.isSaving &&
                    <form onSubmit={(e) => this.handleSalvar(e)}>
                        <p>
                            Nome:
                            <input type="text" name="nome" value={this.state.registro.nome} onChange={this.handleInputChange} />
                        </p>

                        <p>
                            Razão Social:
                            <input type="text" name="razaoSocial" value={this.state.registro.razaoSocial} onChange={this.handleInputChange} />
                        </p>

                        <p>
                            CPF/CNPJ:
                            <input type="text" name="cpfCnpj" value={this.state.registro.cpfCnpj} onChange={this.handleInputChange} />
                        </p>

                        <p>
                            E-mail:
                            <input type="text" name="email" value={this.state.registro.email} onChange={this.handleInputChange} />
                        </p>

                        <button>
                            <Save style={{ color: '#2A9D8F', fontSize: 48 }} />
                        </button>
                    </form>
                }

            </div>
        )
    }
}

const mapStateToProps = state => ({
    empresa: state.empresa
})

const mapDispatchToProps = dispach => ({
    loadByID: (id) => dispach(Actions.editEmpresaRequest(id)),
    save: (id, registro) => dispach(Actions.saveEmpresaRequest(id, registro))
})

export default connect(mapStateToProps, mapDispatchToProps)(EmpresasEditar)