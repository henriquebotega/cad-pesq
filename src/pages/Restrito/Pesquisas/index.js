import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Add, Create, Delete, Description, LocationCity } from '@material-ui/icons';

import Actions from '../../../store/actions'

import './styles.css'

class Pesquisas extends Component {

    componentDidMount() {
        this.props.loadData()
    }

    handleNovo = () => {
        this.props.history.push('/restrito/pesquisas/novo/')
    }

    handlePesqEmpr = (i) => {
        this.props.history.push('/restrito/pesqempr/' + i._id)
    }

    handleEditar = (i) => {
        this.props.history.push('/restrito/pesquisas/editar/' + i._id)
    }

    handlePerguntas = (i) => {
        this.props.history.push('/restrito/perguntas/' + i._id)
    }

    handleExcluir = (i) => {
        this.props.delByID(i._id)
    }

    render() {
        return (
            <div id="box-container">
                <header>
                    <h1>Pesquisas</h1>
                </header>

                <button onClick={() => this.handleNovo()}>
                    <Add style={{ color: '#2A9D8F', fontSize: 48 }} />
                </button>

                {this.props.pesquisa.isLoading &&
                    <div>Carregando...</div>
                }

                <ul>
                    {this.props.pesquisa.registros && this.props.pesquisa.registros.map((item, i) => (
                        <li key={i}>
                            <b>{item.titulo}</b> - {(item._categoria) ? item._categoria.titulo : ''}

                            <span>
                                <button onClick={() => this.handlePesqEmpr(item)}>
                                    <LocationCity style={{ fontSize: 24 }} />
                                </button>

                                <button onClick={() => this.handlePerguntas(item)}>
                                    <Description style={{ fontSize: 24 }} />
                                </button>

                                <button onClick={() => this.handleEditar(item)}>
                                    <Create style={{ fontSize: 24 }} />
                                </button>

                                <button onClick={() => this.handleExcluir(item)}>
                                    <Delete style={{ fontSize: 24 }} />
                                </button>
                            </span>
                        </li>
                    ))}
                </ul>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    pesquisa: state.pesquisa
})

const mapDispatchToProps = dispach => ({
    loadData: () => dispach(Actions.getPesquisasRequest()),
    delByID: (id) => dispach(Actions.delPesquisaRequest(id)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Pesquisas)