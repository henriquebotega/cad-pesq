import React, { Component } from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'

import Admins from '../Admins'
import AdminsEditar from '../AdminsEditar'

import Categorias from '../Categorias'
import CategoriasEditar from '../CategoriasEditar'

import Empresas from '../Empresas'
import EmpresasEditar from '../EmpresasEditar'

import Clientes from '../Clientes'
import ClientesEditar from '../ClientesEditar'

import Pesquisas from '../Pesquisas'
import PesquisasEditar from '../PesquisasEditar'

import PesqEmpr from '../PesqEmpr'
import PesqEmprEditar from '../PesqEmprEditar'

import Perguntas from '../Perguntas'
import PerguntasEditar from '../PerguntasEditar'

import './styles.css'

class Main extends Component {

    state = {
        isLoggedIn: true
    }

    componentDidMount() {
        if (!localStorage.getItem("usuarioLogado")) {
            this.setState({ isLoggedIn: false })
        }
    }

    logout = () => {
        if (localStorage.getItem("usuarioLogado")) {
            localStorage.removeItem("usuarioLogado");
            this.setState({ isLoggedIn: false })
        }
    }

    render() {
        if (!this.state.isLoggedIn) {
            return <Redirect to="/login" />
        }

        return (
            <div id="main-container">
                <h4>Painel de administração</h4>

                <ul id="main-container-ul">
                    <li><a href="/restrito/categorias">Categorias</a></li>
                    <li><a href="/restrito/empresas">Empresas</a></li>
                    <li><a href="/restrito/clientes">Clientes</a></li>
                    <li><a href="/restrito/pesquisas">Pesquisas</a></li>
                    <li><a href="/restrito/admins">Admins</a></li>
                    <li><a href="" onClick={() => this.logout()}>Sair</a></li>
                </ul>

                <div id="main-container-content">
                    <Switch>
                        <Route exact path="/restrito/admins" component={Admins} />
                        <Route path="/restrito/admins/novo" component={AdminsEditar} />
                        <Route path="/restrito/admins/editar/:id" component={AdminsEditar} />

                        <Route exact path="/restrito/categorias" component={Categorias} />
                        <Route path="/restrito/categorias/novo" component={CategoriasEditar} />
                        <Route path="/restrito/categorias/editar/:id" component={CategoriasEditar} />

                        <Route exact path="/restrito/empresas" component={Empresas} />
                        <Route path="/restrito/empresas/novo" component={EmpresasEditar} />
                        <Route path="/restrito/empresas/editar/:id" component={EmpresasEditar} />

                        <Route exact path="/restrito/clientes" component={Clientes} />
                        <Route path="/restrito/clientes/novo" component={ClientesEditar} />
                        <Route path="/restrito/clientes/editar/:id" component={ClientesEditar} />

                        <Route exact path="/restrito/pesquisas" component={Pesquisas} />
                        <Route path="/restrito/pesquisas/novo" component={PesquisasEditar} />
                        <Route path="/restrito/pesquisas/editar/:id" component={PesquisasEditar} />

                        <Route exact path="/restrito/pesqempr/:idPesquisa" component={PesqEmpr} />
                        <Route path="/restrito/pesqempr/:idPesquisa/novo" component={PesqEmprEditar} />
                        <Route path="/restrito/pesqempr/:idPesquisa/editar/:id" component={PesqEmprEditar} />

                        <Route exact path="/restrito/perguntas/:idPesquisa" component={Perguntas} />
                        <Route path="/restrito/perguntas/:idPesquisa/novo" component={PerguntasEditar} />
                        <Route path="/restrito/perguntas/:idPesquisa/editar/:id" component={PerguntasEditar} />
                    </Switch>
                </div>

            </div>
        )
    }
}

export default Main
