import React, { Component } from 'react'
import { connect } from 'react-redux'
import Select from 'react-select'

import Actions from '../../../store/actions'
import { Save } from '@material-ui/icons';

class PesqEmprEditar extends Component {

    state = {
        registro: {
            pesquisa: null,
            _pesquisa: null,
            _empresa: null,
        },
        colEmpresas: []
    }

    componentDidMount() {
        const idPesquisa = this.props.match.params.idPesquisa;
        const id = this.props.match.params.id;

        this.props.loadByID(id)
        this.props.loadPesquisaByID(idPesquisa)
        this.props.loadEmpresas();
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.pesqempr && nextProps.pesqempr.registro && nextProps.pesqempr.registro._id && (nextProps.pesqempr.registro !== this.props.pesqempr.registro)) {
            this.setState({
                registro: {
                    ...this.state.registro,
                    _pesquisa: { ...nextProps.pesqempr.registro._pesquisa },
                    _empresa: { ...nextProps.pesqempr.registro._empresa }
                }
            });
        }

        if (nextProps.pesquisa && nextProps.pesquisa.registro && nextProps.pesquisa.registro.titulo && (nextProps.pesquisa.registro !== this.props.pesquisa.registro)) {
            this.setState({
                registro: {
                    ...this.state.registro,
                    pesquisa: nextProps.pesquisa.registro
                }
            });
        }

        if (nextProps.empresa && nextProps.empresa.registros.length > 0 && (nextProps.empresa.registros !== this.props.empresa.registros)) {
            this.setState({
                colEmpresas: nextProps.empresa.registros.map((regAtual) => { return { value: regAtual._id, label: regAtual.nome } })
            });
        }
    }

    handleSalvar = async (e) => {
        e.preventDefault();

        const idPesquisa = this.props.match.params.idPesquisa;
        const id = this.props.match.params.id;

        const data = {
            _pesquisa: (this.state.registro._pesquisa) ? this.state.registro._pesquisa._id : idPesquisa,
            _empresa: (this.state.registro._empresa._id) ? this.state.registro._empresa._id : this.state.registro._empresa
        }

        this.props.save(id, data)
        this.props.history.push('/restrito/pesqempr/' + idPesquisa)
    }

    handleInputChange = (e) => {
        const { registro } = { ...this.state };
        const currentState = registro;
        const { name, value } = e.target;
        currentState[name] = value;

        this.setState({
            registro: currentState
        })
    }

    handleSelectChange = (e) => {
        this.setState({
            registro: {
                ...this.state.registro,
                _empresa: e.value
            }
        })
    }

    selectOptionValue = () => {
        if (this.state.registro._empresa) {
            const idEmpresa = ((this.state.registro._empresa._id) ? this.state.registro._empresa._id : this.state.registro._empresa)
            return this.state.colEmpresas.filter((regAtual) => regAtual.value === idEmpresa)
        }
    }

    render() {
        return (
            <div id="box-container">
                <header>
                    <h1>Editar Pesquisa</h1>
                </header>

                {this.props.pesquisa.isSaving &&
                    <div>Carregando...</div>
                }

                {!this.props.pesquisa.isSaving &&
                    <form onSubmit={(e) => this.handleSalvar(e)}>

                        <div>
                            Pesquisa:
                            <b>{(this.state.registro.pesquisa) ? this.state.registro.pesquisa.titulo : ''}</b>
                        </div>

                        <div>
                            Empresa:
                            <Select value={this.selectOptionValue()} onChange={this.handleSelectChange} options={this.state.colEmpresas} />
                        </div>

                        <button>
                            <Save style={{ color: '#2A9D8F', fontSize: 48 }} />
                        </button>
                    </form>
                }

            </div>
        )
    }
}

const mapStateToProps = state => ({
    empresa: state.empresa,
    pesquisa: state.pesquisa,
    pesqempr: state.pesqempr
})

const mapDispatchToProps = dispach => ({
    loadEmpresas: () => dispach(Actions.getEmpresasRequest()),
    loadPesquisaByID: (idPesquisa) => dispach(Actions.editPesquisaRequest(idPesquisa)),
    loadByID: (id) => dispach(Actions.editPesqEmprRequest(id)),
    save: (id, registro) => dispach(Actions.savePesqEmprRequest(id, registro))
})

export default connect(mapStateToProps, mapDispatchToProps)(PesqEmprEditar)