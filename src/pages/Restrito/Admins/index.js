import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Add, Create, Delete } from '@material-ui/icons';

import Actions from '../../../store/actions'

import './styles.css'

class Admins extends Component {

    componentDidMount() {
        this.props.loadData()
    }

    handleNovo = () => {
        this.props.history.push('/restrito/admins/novo/')
    }

    handleEditar = (i) => {
        this.props.history.push('/restrito/admins/editar/' + i._id)
    }

    handleExcluir = (i) => {
        this.props.delByID(i._id)
    }

    render() {
        return (
            <div id="box-container">
                <header>
                    <h1>Admins</h1>
                </header>

                <button onClick={() => this.handleNovo()}>
                    <Add style={{ color: '#2A9D8F', fontSize: 48 }} />
                </button>

                {this.props.admin.isLoading &&
                    <div>Carregando...</div>
                }

                <ul>
                    {this.props.admin.registros && this.props.admin.registros.map((item, i) => (
                        <li key={i}>
                            <b>{item.login}</b> - {(item._empresa) ? item._empresa.nome : ''}

                            <span>
                                <button onClick={() => this.handleEditar(item)}>
                                    <Create style={{ fontSize: 24 }} />
                                </button>

                                <button onClick={() => this.handleExcluir(item)}>
                                    <Delete style={{ fontSize: 24 }} />
                                </button>
                            </span>
                        </li>
                    ))}
                </ul>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    admin: state.admin
})

const mapDispatchToProps = dispach => ({
    loadData: () => dispach(Actions.getAdminsRequest()),
    delByID: (id) => dispach(Actions.delAdminRequest(id)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Admins)