import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Add, Create, Delete } from '@material-ui/icons';
import { formataCpfCnpj } from '../../../util';

import Actions from '../../../store/actions'

import './styles.css'

class Empresas extends Component {

    componentDidMount() {
        this.props.loadData()
    }

    handleNovo = () => {
        this.props.history.push('/restrito/empresas/novo/')
    }

    handleEditar = (i) => {
        this.props.history.push('/restrito/empresas/editar/' + i._id)
    }

    handleExcluir = (i) => {
        this.props.delByID(i._id)
    }

    render() {
        return (
            <div id="box-container">
                <header>
                    <h1>Empresas</h1>
                </header>

                <button onClick={() => this.handleNovo()}>
                    <Add style={{ color: '#2A9D8F', fontSize: 48 }} />
                </button>

                {this.props.empresa.isLoading &&
                    <div>Carregando...</div>
                }

                <ul>
                    {this.props.empresa.registros && this.props.empresa.registros.map((item, i) => (
                        <li key={i}>
                            <b>{item.nome}</b> - {formataCpfCnpj(item.cpfCnpj)}

                            <span>
                                <button onClick={() => this.handleEditar(item)}>
                                    <Create style={{ fontSize: 24 }} />
                                </button>

                                <button onClick={() => this.handleExcluir(item)}>
                                    <Delete style={{ fontSize: 24 }} />
                                </button>
                            </span>
                        </li>
                    ))}
                </ul>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    empresa: state.empresa
})

const mapDispatchToProps = dispach => ({
    loadData: () => dispach(Actions.getEmpresasRequest()),
    delByID: (id) => dispach(Actions.delEmpresaRequest(id)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Empresas)