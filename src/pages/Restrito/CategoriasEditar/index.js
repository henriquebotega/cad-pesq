import React, { Component } from 'react'
import { connect } from 'react-redux'

import Actions from '../../../store/actions'
import { Save } from '@material-ui/icons';

class CategoriasEditar extends Component {

    state = {
        registro: {
            titulo: '',
        }
    }

    componentDidMount() {
        const id = this.props.match.params.id;
        this.props.loadByID(id)
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.categoria && nextProps.categoria.registro && nextProps.categoria.registro.titulo) {
            this.setState({
                registro: {
                    ...nextProps.categoria.registro
                }
            });
        }
    }

    handleSalvar = async (e) => {
        e.preventDefault();

        const id = this.props.match.params.id;
        this.props.save(id, this.state.registro)

        this.props.history.push('/restrito/categorias')
    }

    handleInputChange = (e) => {
        const { registro } = { ...this.state };
        const currentState = registro;
        const { name, value } = e.target;
        currentState[name] = value;

        this.setState({
            registro: currentState
        })
    }

    render() {
        return (
            <div id="box-container">
                <header>
                    <h1>Editar Categoria</h1>
                </header>

                {this.props.categoria.isSaving &&
                    <div>Carregando...</div>
                }

                {!this.props.categoria.isSaving &&
                    <form onSubmit={(e) => this.handleSalvar(e)}>
                        <p>
                            Titulo:
                            <input type="text" name="titulo" value={this.state.registro.titulo} onChange={this.handleInputChange} />
                        </p>

                        <button>
                            <Save style={{ color: '#2A9D8F', fontSize: 48 }} />
                        </button>
                    </form>
                }

            </div>
        )
    }
}

const mapStateToProps = state => ({
    categoria: state.categoria
})

const mapDispatchToProps = dispach => ({
    loadByID: (id) => dispach(Actions.editCategoriaRequest(id)),
    save: (id, registro) => dispach(Actions.saveCategoriaRequest(id, registro))
})

export default connect(mapStateToProps, mapDispatchToProps)(CategoriasEditar)