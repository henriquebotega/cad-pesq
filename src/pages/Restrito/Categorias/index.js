import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Add, Create, Delete } from '@material-ui/icons';

import Actions from '../../../store/actions'

import './styles.css'

class Categorias extends Component {

    componentDidMount() {
        this.props.loadData()
    }

    handleNovo = () => {
        this.props.history.push('/restrito/categorias/novo/')
    }

    handleEditar = (i) => {
        this.props.history.push('/restrito/categorias/editar/' + i._id)
    }

    handleExcluir = (i) => {
        this.props.delByID(i._id)
    }

    render() {
        return (
            <div id="box-container">
                <header>
                    <h1>Categorias</h1>
                </header>

                <button onClick={() => this.handleNovo()}>
                    <Add style={{ color: '#2A9D8F', fontSize: 48 }} />
                </button>

                {this.props.categoria.isLoading &&
                    <div>Carregando...</div>
                }

                <ul>
                    {this.props.categoria.registros && this.props.categoria.registros.map((item, i) => (
                        <li key={i}>
                            <b>{item.titulo}</b>

                            <span>
                                <button onClick={() => this.handleEditar(item)}>
                                    <Create style={{ fontSize: 24 }} />
                                </button>

                                <button onClick={() => this.handleExcluir(item)}>
                                    <Delete style={{ fontSize: 24 }} />
                                </button>
                            </span>
                        </li>
                    ))}
                </ul>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    categoria: state.categoria
})

const mapDispatchToProps = dispach => ({
    loadData: () => dispach(Actions.getCategoriasRequest()),
    delByID: (id) => dispach(Actions.delCategoriaRequest(id)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Categorias)