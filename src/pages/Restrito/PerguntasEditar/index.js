import React, { Component } from 'react'
import { connect } from 'react-redux'
import Select from 'react-select'

import Actions from '../../../store/actions'
import { Save } from '@material-ui/icons';

class PerguntasEditar extends Component {

    state = {
        registro: {
            pesquisa: null,
            _pesquisa: null
        }
    }

    componentDidMount() {
        const idPesquisa = this.props.match.params.idPesquisa;
        const id = this.props.match.params.id;

        this.props.loadByID(id)
        this.props.loadPesquisaByID(idPesquisa)
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.pergunta && nextProps.pergunta.registro && nextProps.pergunta.registro._id && (nextProps.pergunta.registro !== this.props.pergunta.registro)) {
            this.setState({
                registro: {
                    ...this.state.registro,
                    _pesquisa: { ...nextProps.pergunta.registro._pesquisa }
                }
            });
        }

        if (nextProps.pesquisa && nextProps.pesquisa.registro && nextProps.pesquisa.registro.titulo && (nextProps.pesquisa.registro !== this.props.pesquisa.registro)) {
            this.setState({
                registro: {
                    ...this.state.registro,
                    pesquisa: nextProps.pesquisa.registro
                }
            });
        }

    }

    handleSalvar = async (e) => {
        e.preventDefault();

        const idPesquisa = this.props.match.params.idPesquisa;
        const id = this.props.match.params.id;

        const data = {
            _pesquisa: (this.state.registro._pesquisa) ? this.state.registro._pesquisa._id : idPesquisa,
        }

        this.props.save(id, data)
        this.props.history.push('/restrito/perguntas/' + idPesquisa)
    }

    handleInputChange = (e) => {
        const { registro } = { ...this.state };
        const currentState = registro;
        const { name, value } = e.target;
        currentState[name] = value;

        this.setState({
            registro: currentState
        })
    }

    render() {
        return (
            <div id="box-container">
                <header>
                    <h1>Editar Pergunta</h1>
                </header>

                {this.props.pesquisa.isSaving &&
                    <div>Carregando...</div>
                }

                {!this.props.pesquisa.isSaving &&
                    <form onSubmit={(e) => this.handleSalvar(e)}>

                        <div>
                            Pesquisa:
                            <b>{(this.state.registro.pesquisa) ? this.state.registro.pesquisa.titulo : ''}</b>
                        </div>

                        <p>
                            Titulo:
                            <input type="text" name="titulo" value={this.state.registro.titulo} onChange={this.handleInputChange} />
                        </p>

                        <button>
                            <Save style={{ color: '#2A9D8F', fontSize: 48 }} />
                        </button>
                    </form>
                }

            </div>
        )
    }
}

const mapStateToProps = state => ({
    pesquisa: state.pesquisa,
    pergunta: state.pergunta
})

const mapDispatchToProps = dispach => ({
    loadPesquisaByID: (idPesquisa) => dispach(Actions.editPesquisaRequest(idPesquisa)),
    loadByID: (id) => dispach(Actions.editPerguntaRequest(id)),
    save: (id, registro) => dispach(Actions.savePerguntaRequest(id, registro))
})

export default connect(mapStateToProps, mapDispatchToProps)(PerguntasEditar)