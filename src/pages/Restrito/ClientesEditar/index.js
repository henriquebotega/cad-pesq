import React, { Component } from 'react'
import { connect } from 'react-redux'

import Actions from '../../../store/actions'
import { Save } from '@material-ui/icons';

class ClientesEditar extends Component {

    state = {
        registro: {
            nome: '',
            cpf: ''
        }
    }

    componentDidMount() {
        const id = this.props.match.params.id;
        this.props.loadByID(id)
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.cliente && nextProps.cliente.registro && nextProps.cliente.registro.nome) {
            this.setState({
                registro: {
                    ...nextProps.cliente.registro
                }
            });
        }
    }

    handleSalvar = async (e) => {
        e.preventDefault();

        const id = this.props.match.params.id;
        this.props.save(id, this.state.registro)

        this.props.history.push('/restrito/clientes')
    }

    handleInputChange = (e) => {
        const { registro } = { ...this.state };
        const currentState = registro;
        const { name, value } = e.target;
        currentState[name] = value;

        this.setState({
            registro: currentState
        })
    }

    render() {
        return (
            <div id="box-container">
                <header>
                    <h1>Editar Cliente</h1>
                </header>

                {this.props.cliente.isSaving &&
                    <div>Carregando...</div>
                }

                {!this.props.cliente.isSaving &&
                    <form onSubmit={(e) => this.handleSalvar(e)}>
                        <p>
                            Nome:
                            <input type="text" name="nome" value={this.state.registro.nome} onChange={this.handleInputChange} />
                        </p>

                        <p>
                            CPF:
                            <input type="text" name="cpf" value={this.state.registro.cpf} onChange={this.handleInputChange} />
                        </p>

                        <button>
                            <Save style={{ color: '#2A9D8F', fontSize: 48 }} />
                        </button>
                    </form>
                }

            </div>
        )
    }
}

const mapStateToProps = state => ({
    cliente: state.cliente
})

const mapDispatchToProps = dispach => ({
    loadByID: (id) => dispach(Actions.editClienteRequest(id)),
    save: (id, registro) => dispach(Actions.saveClienteRequest(id, registro))
})

export default connect(mapStateToProps, mapDispatchToProps)(ClientesEditar)