import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Add, Create, Delete } from '@material-ui/icons';

import Actions from '../../../store/actions'

import './styles.css'

class Clientes extends Component {

    componentDidMount() {
        this.props.loadData()
    }

    handleNovo = () => {
        this.props.history.push('/restrito/clientes/novo/')
    }

    handleEditar = (i) => {
        this.props.history.push('/restrito/clientes/editar/' + i._id)
    }

    handleExcluir = (i) => {
        this.props.delByID(i._id)
    }

    render() {
        return (
            <div id="box-container">
                <header>
                    <h1>Clientes</h1>
                </header>

                <button onClick={() => this.handleNovo()}>
                    <Add style={{ color: '#2A9D8F', fontSize: 48 }} />
                </button>

                {this.props.cliente.isLoading &&
                    <div>Carregando...</div>
                }

                <ul>
                    {this.props.cliente.registros && this.props.cliente.registros.map((item, i) => (
                        <li key={i}>
                            <b>{item.nome}</b>

                            <span>
                                <button onClick={() => this.handleEditar(item)}>
                                    <Create style={{ fontSize: 24 }} />
                                </button>

                                <button onClick={() => this.handleExcluir(item)}>
                                    <Delete style={{ fontSize: 24 }} />
                                </button>
                            </span>
                        </li>
                    ))}
                </ul>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    cliente: state.cliente
})

const mapDispatchToProps = dispach => ({
    loadData: () => dispach(Actions.getClientesRequest()),
    delByID: (id) => dispach(Actions.delClienteRequest(id)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Clientes)