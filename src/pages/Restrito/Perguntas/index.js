import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Add, Create, Delete, InsertChartOutlined } from '@material-ui/icons';

import Actions from '../../../store/actions'

import './styles.css'

class Perguntas extends Component {

    componentDidMount() {
        const idPesquisa = this.props.match.params.idPesquisa;

        this.props.loadData(idPesquisa);
        this.props.loadPesquisaByID(idPesquisa);
    }

    handleNovo = () => {
        const idPesquisa = this.props.match.params.idPesquisa;
        this.props.history.push('/restrito/perguntas/' + idPesquisa + '/novo/')
    }

    handleEditar = (i) => {
        const idPesquisa = this.props.match.params.idPesquisa;
        this.props.history.push('/restrito/perguntas/' + idPesquisa + '/editar/' + i._id)
    }

    handleExcluir = (i) => {
        this.props.delByID(i._id)
    }

    render() {
        return (
            <div id="box-container">
                <header>
                    <h1>Perguntas por Pesquisa</h1>
                </header>

                <h3>{this.props.pesquisa.registro.titulo}</h3>

                <button onClick={() => this.handleNovo()}>
                    <Add style={{ color: '#2A9D8F', fontSize: 48 }} />
                </button>

                {this.props.pergunta.isLoading &&
                    <div>Carregando...</div>
                }

                <ul>
                    {this.props.pergunta.registros && this.props.pergunta.registros.map((item, i) => (
                        <li key={i}>
                            {item.titulo}

                            <span>
                                <button onClick={() => this.handleEditar(item)}>
                                    <Create style={{ fontSize: 24 }} />
                                </button>

                                <button onClick={() => this.handleExcluir(item)}>
                                    <Delete style={{ fontSize: 24 }} />
                                </button>
                            </span>
                        </li>
                    ))}
                </ul>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    pesquisa: state.pesquisa,
    pergunta: state.pergunta
})

const mapDispatchToProps = dispach => ({
    loadPesquisaByID: (idPesquisa) => dispach(Actions.editPesquisaRequest(idPesquisa)),
    loadData: (idPesquisa) => dispach(Actions.getPerguntasRequest(idPesquisa)),
    delByID: (id) => dispach(Actions.delPerguntaRequest(id)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Perguntas)