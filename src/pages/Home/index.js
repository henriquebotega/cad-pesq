import React, { Component } from 'react'

class Home extends Component {
    render() {
        return (
            <div id="main-container">
                <h4>Tela Home</h4>

                <ul>
                    <li><a href="/">Principal</a></li>
                    <li><a href="/login">Login</a></li>
                </ul>
            </div>
        )
    }
}

export default Home
