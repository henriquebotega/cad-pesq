import React, { Component } from 'react';
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import { isNotBlank } from '../../util';

import Actions from '../../store/actions'

class Login extends Component {
    state = {
        usuario: '',
        password: ''
    }

    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    validar = () => {
        this.props.validar(this.state.usuario, this.state.password)
    }

    render() {
        if (localStorage.getItem("usuarioLogado")) {
            return <Redirect to='/restrito' />
        }

        if (!this.props.login.isLoading && isNotBlank(this.props.login.data['data'])) {
            return <Redirect to='/restrito' />
        }

        return (
            <div>
                Login/E-mail:
                <input type="text" value={this.state.usuario} name="usuario" onChange={(e) => this.handleChange(e)} /> <br /><br />

                Password:
                <input type="password" value={this.state.password} name="password" onChange={(e) => this.handleChange(e)} />

                {this.props.login.isLoading &&
                    <div>Aguarde, carregando...</div>
                }

                {!this.props.login.isLoading &&
                    <button onClick={() => this.validar()}>Validar</button>
                }

                {!this.props.login.isLoading && this.props.login.error &&
                    <div>{this.props.login.errorMsg}</div>
                }
            </div>
        )
    }
}

const mapStateToProps = state => ({
    login: state.login
})

const mapDispatchToProps = dispach => ({
    validar: (usuario, password) => dispach(Actions.getLoginRequest(usuario, password))
})

export default connect(mapStateToProps, mapDispatchToProps)(Login)