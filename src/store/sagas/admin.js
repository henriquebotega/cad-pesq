import Actions from '../actions'
import { put, call } from 'redux-saga/effects'
import { api } from '../../services/api';

export function* getAdmins() {
    try {
        const res = yield call(api.get, "/admins");
        yield put(Actions.getAdminsSuccess(res.data))
    } catch (error) {
        yield put(Actions.getAdminsFailure(error))
    }
}

export function* editAdmin(data) {
    try {

        if (data.id) {
            const res = yield call(api.get, "/admins/" + data.id);
            yield put(Actions.editAdminSuccess(res.data))
        } else {
            yield put(Actions.editAdminSuccess())
        }

    } catch (error) {
        yield put(Actions.editAdminFailure(error))
    }
}

export function* delAdmin(data) {
    try {
        yield call(api.delete, "/admins/" + data.id);
        yield put(Actions.delAdminSuccess(data.id))
    } catch (error) {
        yield put(Actions.delAdminFailure(error))
    }
}

export function* saveAdmin(data) {
    try {

        if (data.id) {
            const res = yield call(api.put, "/admins/" + data.id, data.data);
            yield put(Actions.saveAdminSuccess(res.data))
        } else {
            const res = yield call(api.post, "/admins/", data.data);
            yield put(Actions.saveAdminSuccess(res.data))
        }

    } catch (error) {
        yield put(Actions.saveAdminFailure(error))
    }
}