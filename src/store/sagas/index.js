import { takeLatest, all } from 'redux-saga/effects'
import { Types } from '../actions'

import { getLogin } from './login';
import { getAdmins, editAdmin, delAdmin, saveAdmin } from './admin';
import { getCategorias, editCategoria, delCategoria, saveCategoria } from './categoria';
import { getEmpresas, editEmpresa, delEmpresa, saveEmpresa } from './empresa';
import { getClientes, editCliente, delCliente, saveCliente } from './cliente';
import { getPesquisas, editPesquisa, delPesquisa, savePesquisa } from './pesquisa';
import { getPesqEmpr, editPesqEmpr, delPesqEmpr, savePesqEmpr } from './pesqempr';
import { getPerguntas, editPergunta, delPergunta, savePergunta } from './pergunta';

export default function* rootSaga() {
    yield all([
        takeLatest(Types.GET_LOGIN_REQUEST, getLogin),

        takeLatest(Types.GET_ADMINS_REQUEST, getAdmins),
        takeLatest(Types.EDIT_ADMIN_REQUEST, editAdmin),
        takeLatest(Types.DEL_ADMIN_REQUEST, delAdmin),
        takeLatest(Types.SAVE_ADMIN_REQUEST, saveAdmin),

        takeLatest(Types.GET_CATEGORIAS_REQUEST, getCategorias),
        takeLatest(Types.EDIT_CATEGORIA_REQUEST, editCategoria),
        takeLatest(Types.DEL_CATEGORIA_REQUEST, delCategoria),
        takeLatest(Types.SAVE_CATEGORIA_REQUEST, saveCategoria),

        takeLatest(Types.GET_EMPRESAS_REQUEST, getEmpresas),
        takeLatest(Types.EDIT_EMPRESA_REQUEST, editEmpresa),
        takeLatest(Types.DEL_EMPRESA_REQUEST, delEmpresa),
        takeLatest(Types.SAVE_EMPRESA_REQUEST, saveEmpresa),

        takeLatest(Types.GET_CLIENTES_REQUEST, getClientes),
        takeLatest(Types.EDIT_CLIENTE_REQUEST, editCliente),
        takeLatest(Types.DEL_CLIENTE_REQUEST, delCliente),
        takeLatest(Types.SAVE_CLIENTE_REQUEST, saveCliente),

        takeLatest(Types.GET_PESQUISAS_REQUEST, getPesquisas),
        takeLatest(Types.EDIT_PESQUISA_REQUEST, editPesquisa),
        takeLatest(Types.DEL_PESQUISA_REQUEST, delPesquisa),
        takeLatest(Types.SAVE_PESQUISA_REQUEST, savePesquisa),

        takeLatest(Types.GET_PESQ_EMPR_REQUEST, getPesqEmpr),
        takeLatest(Types.EDIT_PESQ_EMPR_REQUEST, editPesqEmpr),
        takeLatest(Types.DEL_PESQ_EMPR_REQUEST, delPesqEmpr),
        takeLatest(Types.SAVE_PESQ_EMPR_REQUEST, savePesqEmpr),

        takeLatest(Types.GET_PERGUNTAS_REQUEST, getPerguntas),
        takeLatest(Types.EDIT_PERGUNTA_REQUEST, editPergunta),
        takeLatest(Types.DEL_PERGUNTA_REQUEST, delPergunta),
        takeLatest(Types.SAVE_PERGUNTA_REQUEST, savePergunta),
    ])
}