import Actions from '../actions'
import { put, call } from 'redux-saga/effects'
import { api } from '../../services/api';

export function* getLogin(data) {

    let params = {
        usuario: data.usuario,
        password: data.password
    }

    try {
        const res = yield call(api.post, "/admins/validar", params)

        if (res && res.data.length > 0) {
            localStorage.setItem('usuarioLogado', JSON.stringify(res.data))

            yield put(Actions.getLoginSuccess(res))
        } else {
            localStorage.removeItem('usuarioLogado')
            yield put(Actions.getLoginFailure('Nenhum registro encontrado'))
        }

    } catch (error) {
        localStorage.removeItem('usuarioLogado')
        yield put(Actions.getLoginFailure('Serviço fora do ar.'))
    }
}