import Actions from '../actions'
import { put, call } from 'redux-saga/effects'
import { api } from '../../services/api';

export function* getPesqEmpr(data) {
    try {
        const res = yield call(api.get, "/pesqempr/pesquisa/" + data.idPesquisa);
        yield put(Actions.getPesqEmprSuccess(res.data))
    } catch (error) {
        yield put(Actions.getPesqEmprFailure(error))
    }
}

export function* editPesqEmpr(data) {
    try {

        if (data.id) {
            const res = yield call(api.get, "/pesqempr/" + data.id);
            yield put(Actions.editPesqEmprSuccess(res.data))
        } else {
            yield put(Actions.editPesqEmprSuccess())
        }

    } catch (error) {
        yield put(Actions.editPesquisaFailure(error))
    }
}

export function* delPesqEmpr(data) {
    try {
        yield call(api.delete, "/pesqempr/" + data.id);
        yield put(Actions.delPesqEmprSuccess(data.id))
    } catch (error) {
        yield put(Actions.delPesquisaFailure(error))
    }
}

export function* savePesqEmpr(data) {
    try {

        if (data.id) {
            const res = yield call(api.put, "/pesqempr/" + data.id, data.data);
            yield put(Actions.savePesqEmprSuccess(res.data))
        } else {
            const res = yield call(api.post, "/pesqempr/", data.data);
            yield put(Actions.savePesqEmprSuccess(res.data))
        }

    } catch (error) {
        yield put(Actions.savePesquisaFailure(error))
    }
}