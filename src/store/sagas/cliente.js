import Actions from '../actions'
import { put, call } from 'redux-saga/effects'
import { api } from '../../services/api';

export function* getClientes() {
    try {
        const res = yield call(api.get, "/clientes");
        yield put(Actions.getClientesSuccess(res.data))
    } catch (error) {
        yield put(Actions.getClientesFailure(error))
    }
}

export function* editCliente(data) {
    try {

        if (data.id) {
            const res = yield call(api.get, "/clientes/" + data.id);
            yield put(Actions.editClienteSuccess(res.data))
        } else {
            yield put(Actions.editClienteSuccess())
        }

    } catch (error) {
        yield put(Actions.editClienteFailure(error))
    }
}

export function* delCliente(data) {
    try {
        yield call(api.delete, "/clientes/" + data.id);
        yield put(Actions.delClienteSuccess(data.id))
    } catch (error) {
        yield put(Actions.delClienteFailure(error))
    }
}

export function* saveCliente(data) {
    try {

        if (data.id) {
            const res = yield call(api.put, "/clientes/" + data.id, data.data);
            yield put(Actions.saveClienteSuccess(res.data))
        } else {
            const res = yield call(api.post, "/clientes/", data.data);
            yield put(Actions.saveClienteSuccess(res.data))
        }

    } catch (error) {
        yield put(Actions.saveClienteFailure(error))
    }
}