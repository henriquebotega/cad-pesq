import Actions from '../actions'
import { put, call } from 'redux-saga/effects'
import { api } from '../../services/api';

export function* getPesquisas() {
    try {
        const res = yield call(api.get, "/pesquisas");
        yield put(Actions.getPesquisasSuccess(res.data))
    } catch (error) {
        yield put(Actions.getPesquisasFailure(error))
    }
}

export function* editPesquisa(data) {
    try {

        if (data.id) {
            const res = yield call(api.get, "/pesquisas/" + data.id);
            yield put(Actions.editPesquisaSuccess(res.data))
        } else {
            yield put(Actions.editPesquisaSuccess())
        }

    } catch (error) {
        yield put(Actions.editPesquisaFailure(error))
    }
}

export function* delPesquisa(data) {
    try {
        yield call(api.delete, "/pesquisas/" + data.id);
        yield put(Actions.delPesquisaSuccess(data.id))
    } catch (error) {
        yield put(Actions.delPesquisaFailure(error))
    }
}

export function* savePesquisa(data) {
    try {

        if (data.id) {
            const res = yield call(api.put, "/pesquisas/" + data.id, data.data);
            yield put(Actions.savePesquisaSuccess(res.data))
        } else {
            const res = yield call(api.post, "/pesquisas/", data.data);
            yield put(Actions.savePesquisaSuccess(res.data))
        }

    } catch (error) {
        yield put(Actions.savePesquisaFailure(error))
    }
}