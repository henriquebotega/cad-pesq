import Actions from '../actions'
import { put, call } from 'redux-saga/effects'
import { api } from '../../services/api';

export function* getCategorias() {
    try {
        const res = yield call(api.get, "/categorias");
        yield put(Actions.getCategoriasSuccess(res.data))
    } catch (error) {
        yield put(Actions.getCategoriasFailure(error))
    }
}

export function* editCategoria(data) {
    try {

        if (data.id) {
            const res = yield call(api.get, "/categorias/" + data.id);
            yield put(Actions.editCategoriaSuccess(res.data))
        } else {
            yield put(Actions.editCategoriaSuccess())
        }

    } catch (error) {
        yield put(Actions.editCategoriaFailure(error))
    }
}

export function* delCategoria(data) {
    try {
        yield call(api.delete, "/categorias/" + data.id);
        yield put(Actions.delCategoriaSuccess(data.id))
    } catch (error) {
        yield put(Actions.delCategoriaFailure(error))
    }
}

export function* saveCategoria(data) {
    try {

        if (data.id) {
            const res = yield call(api.put, "/categorias/" + data.id, data.data);
            yield put(Actions.saveCategoriaSuccess(res.data))
        } else {
            const res = yield call(api.post, "/categorias/", data.data);
            yield put(Actions.saveCategoriaSuccess(res.data))
        }

    } catch (error) {
        yield put(Actions.saveCategoriaFailure(error))
    }
}