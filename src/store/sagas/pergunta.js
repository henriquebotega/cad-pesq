import Actions from '../actions'
import { put, call } from 'redux-saga/effects'
import { api } from '../../services/api';

export function* getPerguntas(data) {
    try {
        const res = yield call(api.get, "/perguntas/pesquisa/" + data.idPesquisa);
        yield put(Actions.getPerguntasSuccess(res.data))
    } catch (error) {
        yield put(Actions.getPerguntasFailure(error))
    }
}

export function* editPergunta(data) {
    try {

        if (data.id) {
            const res = yield call(api.get, "/perguntas/" + data.id);
            yield put(Actions.editPerguntaSuccess(res.data))
        } else {
            yield put(Actions.editPerguntaSuccess())
        }

    } catch (error) {
        yield put(Actions.editPesquisaFailure(error))
    }
}

export function* delPergunta(data) {
    try {
        yield call(api.delete, "/perguntas/" + data.id);
        yield put(Actions.delPerguntaSuccess(data.id))
    } catch (error) {
        yield put(Actions.delPesquisaFailure(error))
    }
}

export function* savePergunta(data) {
    try {

        if (data.id) {
            const res = yield call(api.put, "/perguntas/" + data.id, data.data);
            yield put(Actions.savePerguntaSuccess(res.data))
        } else {
            const res = yield call(api.post, "/perguntas/", data.data);
            yield put(Actions.savePerguntaSuccess(res.data))
        }

    } catch (error) {
        yield put(Actions.savePesquisaFailure(error))
    }
}