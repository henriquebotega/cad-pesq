import Actions from '../actions'
import { put, call } from 'redux-saga/effects'
import { api } from '../../services/api';

export function* getEmpresas() {
    try {
        const res = yield call(api.get, "/empresas");
        yield put(Actions.getEmpresasSuccess(res.data))
    } catch (error) {
        yield put(Actions.getEmpresasFailure(error))
    }
}

export function* editEmpresa(data) {
    try {

        if (data.id) {
            const res = yield call(api.get, "/empresas/" + data.id);
            yield put(Actions.editEmpresaSuccess(res.data))
        } else {
            yield put(Actions.editEmpresaSuccess())
        }

    } catch (error) {
        yield put(Actions.editEmpresaFailure(error))
    }
}

export function* delEmpresa(data) {
    try {
        yield call(api.delete, "/empresas/" + data.id);
        yield put(Actions.delEmpresaSuccess(data.id))
    } catch (error) {
        yield put(Actions.delEmpresaFailure(error))
    }
}

export function* saveEmpresa(data) {
    try {

        if (data.id) {
            const res = yield call(api.put, "/empresas/" + data.id, data.data);
            yield put(Actions.saveEmpresaSuccess(res.data))
        } else {
            const res = yield call(api.post, "/empresas/", data.data);
            yield put(Actions.saveEmpresaSuccess(res.data))
        }

    } catch (error) {
        yield put(Actions.saveEmpresaFailure(error))
    }
}