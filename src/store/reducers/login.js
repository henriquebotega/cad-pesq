import { Types } from '../actions'
import { createReducer } from 'reduxsauce';

export const INITIAL_STATE = {
    isLoading: false,
    data: {},
    error: false,
    errorMsg: ''
}

export const getLoginRequest = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isLoading: true,
        error: false,
        errorMsg: ''
    }
}

export const getLoginSuccess = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isLoading: false,
        data: action.data
    }
}

export const getLoginFailure = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isLoading: false,
        error: true,
        errorMsg: action.error,
    }
}

export const HANDLERS = {
    [Types.GET_LOGIN_REQUEST]: getLoginRequest,
    [Types.GET_LOGIN_SUCCESS]: getLoginSuccess,
    [Types.GET_LOGIN_FAILURE]: getLoginFailure,
}

export default createReducer(INITIAL_STATE, HANDLERS)