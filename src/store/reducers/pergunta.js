import { Types } from '../actions'
import { createReducer } from 'reduxsauce';

export const INITIAL_STATE = {
    isLoading: false,
    registros: [],
    isSaving: false,
    registro: {}
}

export const getPerguntasRequest = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isLoading: true
    }
}

export const getPerguntasSuccess = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isLoading: false,
        registros: action.data
    }
}

export const getPerguntasFailure = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isLoading: false
    }
}

export const editPerguntaRequest = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isSaving: true
    }
}

export const editPerguntaSuccess = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isSaving: false,
        registro: action.data
    }
}

export const editPerguntaFailure = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isSaving: false
    }
}

export const delPerguntaRequest = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isLoading: true
    }
}

export const delPerguntaSuccess = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isLoading: false,
        registros: [...state.registros.filter(item => { return item._id !== action.id })]
    }
}

export const delPerguntaFailure = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isLoading: false
    }
}

export const savePerguntaRequest = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isSaving: true
    }
}

export const savePerguntaSuccess = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isSaving: false,
        registros: [...state.registros.filter(item => { return item._id !== action.data._id }), action.data]
    }
}

export const savePerguntaFailure = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isSaving: false
    }
}

export const HANDLERS = {
    [Types.GET_PERGUNTAS_REQUEST]: getPerguntasRequest,
    [Types.GET_PERGUNTAS_SUCCESS]: getPerguntasSuccess,
    [Types.GET_PERGUNTAS_FAILURE]: getPerguntasFailure,

    [Types.EDIT_PERGUNTA_REQUEST]: editPerguntaRequest,
    [Types.EDIT_PERGUNTA_SUCCESS]: editPerguntaSuccess,
    [Types.EDIT_PERGUNTA_FAILURE]: editPerguntaFailure,

    [Types.DEL_PERGUNTA_REQUEST]: delPerguntaRequest,
    [Types.DEL_PERGUNTA_SUCCESS]: delPerguntaSuccess,
    [Types.DEL_PERGUNTA_FAILURE]: delPerguntaFailure,

    [Types.SAVE_PERGUNTA_REQUEST]: savePerguntaRequest,
    [Types.SAVE_PERGUNTA_SUCCESS]: savePerguntaSuccess,
    [Types.SAVE_PERGUNTA_FAILURE]: savePerguntaFailure,
}

export default createReducer(INITIAL_STATE, HANDLERS)