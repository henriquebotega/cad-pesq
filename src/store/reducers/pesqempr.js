import { Types } from '../actions'
import { createReducer } from 'reduxsauce';

export const INITIAL_STATE = {
    isLoading: false,
    registros: [],
    isSaving: false,
    registro: {}
}

export const getPesqEmprRequest = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isLoading: true
    }
}

export const getPesqEmprSuccess = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isLoading: false,
        registros: action.data
    }
}

export const getPesqEmprFailure = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isLoading: false
    }
}

export const editPesqEmprRequest = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isSaving: true
    }
}

export const editPesqEmprSuccess = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isSaving: false,
        registro: action.data
    }
}

export const editPesqEmprFailure = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isSaving: false
    }
}

export const delPesqEmprRequest = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isLoading: true
    }
}

export const delPesqEmprSuccess = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isLoading: false,
        registros: [...state.registros.filter(item => { return item._id !== action.id })]
    }
}

export const delPesqEmprFailure = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isLoading: false
    }
}

export const savePesqEmprRequest = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isSaving: true
    }
}

export const savePesqEmprSuccess = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isSaving: false,
        registros: [...state.registros.filter(item => { return item._id !== action.data._id }), action.data]
    }
}

export const savePesqEmprFailure = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isSaving: false
    }
}

export const HANDLERS = {
    [Types.GET_PESQ_EMPR_REQUEST]: getPesqEmprRequest,
    [Types.GET_PESQ_EMPR_SUCCESS]: getPesqEmprSuccess,
    [Types.GET_PESQ_EMPR_FAILURE]: getPesqEmprFailure,

    [Types.EDIT_PESQ_EMPR_REQUEST]: editPesqEmprRequest,
    [Types.EDIT_PESQ_EMPR_SUCCESS]: editPesqEmprSuccess,
    [Types.EDIT_PESQ_EMPR_FAILURE]: editPesqEmprFailure,

    [Types.DEL_PESQ_EMPR_REQUEST]: delPesqEmprRequest,
    [Types.DEL_PESQ_EMPR_SUCCESS]: delPesqEmprSuccess,
    [Types.DEL_PESQ_EMPR_FAILURE]: delPesqEmprFailure,

    [Types.SAVE_PESQ_EMPR_REQUEST]: savePesqEmprRequest,
    [Types.SAVE_PESQ_EMPR_SUCCESS]: savePesqEmprSuccess,
    [Types.SAVE_PESQ_EMPR_FAILURE]: savePesqEmprFailure,
}

export default createReducer(INITIAL_STATE, HANDLERS)