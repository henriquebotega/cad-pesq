import { Types } from '../actions'
import { createReducer } from 'reduxsauce';

export const INITIAL_STATE = {
    isLoading: false,
    registros: [],
    isSaving: false,
    registro: {}
}

export const getClientesRequest = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isLoading: true
    }
}

export const getClientesSuccess = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isLoading: false,
        registros: action.data
    }
}

export const getClientesFailure = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isLoading: false
    }
}

export const editClienteRequest = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isSaving: true
    }
}

export const editClienteSuccess = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isSaving: false,
        registro: action.data
    }
}

export const editClienteFailure = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isSaving: false
    }
}

export const delClienteRequest = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isLoading: true
    }
}

export const delClienteSuccess = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isLoading: false,
        registros: [...state.registros.filter(item => { return item._id !== action.id })]
    }
}

export const delClienteFailure = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isLoading: false
    }
}

export const saveClienteRequest = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isSaving: true
    }
}

export const saveClienteSuccess = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isSaving: false,
        registros: [...state.registros.filter(item => { return item._id !== action.data._id }), action.data]
    }
}

export const saveClienteFailure = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isSaving: false
    }
}

export const HANDLERS = {
    [Types.GET_CLIENTES_REQUEST]: getClientesRequest,
    [Types.GET_CLIENTES_SUCCESS]: getClientesSuccess,
    [Types.GET_CLIENTES_FAILURE]: getClientesFailure,

    [Types.EDIT_CLIENTE_REQUEST]: editClienteRequest,
    [Types.EDIT_CLIENTE_SUCCESS]: editClienteSuccess,
    [Types.EDIT_CLIENTE_FAILURE]: editClienteFailure,

    [Types.DEL_CLIENTE_REQUEST]: delClienteRequest,
    [Types.DEL_CLIENTE_SUCCESS]: delClienteSuccess,
    [Types.DEL_CLIENTE_FAILURE]: delClienteFailure,

    [Types.SAVE_CLIENTE_REQUEST]: saveClienteRequest,
    [Types.SAVE_CLIENTE_SUCCESS]: saveClienteSuccess,
    [Types.SAVE_CLIENTE_FAILURE]: saveClienteFailure,
}

export default createReducer(INITIAL_STATE, HANDLERS)