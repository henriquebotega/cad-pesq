import { Types } from '../actions'
import { createReducer } from 'reduxsauce';

export const INITIAL_STATE = {
    isLoading: false,
    registros: [],
    isSaving: false,
    registro: {}
}

export const getAdminsRequest = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isLoading: true
    }
}

export const getAdminsSuccess = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isLoading: false,
        registros: action.data
    }
}

export const getAdminsFailure = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isLoading: false
    }
}

export const editAdminRequest = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isSaving: true
    }
}

export const editAdminSuccess = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isSaving: false,
        registro: action.data
    }
}

export const editAdminFailure = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isSaving: false
    }
}

export const delAdminRequest = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isLoading: true
    }
}

export const delAdminSuccess = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isLoading: false,
        registros: [...state.registros.filter(item => { return item._id !== action.id })]
    }
}

export const delAdminFailure = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isLoading: false
    }
}

export const saveAdminRequest = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isSaving: true
    }
}

export const saveAdminSuccess = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isSaving: false,
        registros: [...state.registros.filter(item => { return item._id !== action.data._id }), action.data]
    }
}

export const saveAdminFailure = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isSaving: false
    }
}

export const HANDLERS = {
    [Types.GET_ADMINS_REQUEST]: getAdminsRequest,
    [Types.GET_ADMINS_SUCCESS]: getAdminsSuccess,
    [Types.GET_ADMINS_FAILURE]: getAdminsFailure,

    [Types.EDIT_ADMIN_REQUEST]: editAdminRequest,
    [Types.EDIT_ADMIN_SUCCESS]: editAdminSuccess,
    [Types.EDIT_ADMIN_FAILURE]: editAdminFailure,

    [Types.DEL_ADMIN_REQUEST]: delAdminRequest,
    [Types.DEL_ADMIN_SUCCESS]: delAdminSuccess,
    [Types.DEL_ADMIN_FAILURE]: delAdminFailure,

    [Types.SAVE_ADMIN_REQUEST]: saveAdminRequest,
    [Types.SAVE_ADMIN_SUCCESS]: saveAdminSuccess,
    [Types.SAVE_ADMIN_FAILURE]: saveAdminFailure,
}

export default createReducer(INITIAL_STATE, HANDLERS)