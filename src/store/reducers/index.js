import { combineReducers } from 'redux'

import login from './login'
import admin from './admin'
import categoria from './categoria'
import empresa from './empresa'
import cliente from './cliente'
import pesquisa from './pesquisa'
import pesqempr from './pesqempr'
import pergunta from './pergunta'

const rootReducer = combineReducers({
    login,
    admin,
    categoria,
    empresa,
    cliente,
    pesquisa,
    pesqempr,
    pergunta,
})

export default rootReducer