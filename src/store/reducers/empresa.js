import { Types } from '../actions'
import { createReducer } from 'reduxsauce';

export const INITIAL_STATE = {
    isLoading: false,
    registros: [],
    isSaving: false,
    registro: {}
}

export const getEmpresasRequest = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isLoading: true
    }
}

export const getEmpresasSuccess = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isLoading: false,
        registros: action.data
    }
}

export const getEmpresasFailure = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isLoading: false
    }
}

export const editEmpresaRequest = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isSaving: true
    }
}

export const editEmpresaSuccess = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isSaving: false,
        registro: action.data
    }
}

export const editEmpresaFailure = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isSaving: false
    }
}

export const delEmpresaRequest = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isLoading: true
    }
}

export const delEmpresaSuccess = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isLoading: false,
        registros: [...state.registros.filter(item => { return item._id !== action.id })]
    }
}

export const delEmpresaFailure = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isLoading: false
    }
}

export const saveEmpresaRequest = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isSaving: true
    }
}

export const saveEmpresaSuccess = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isSaving: false,
        registros: [...state.registros.filter(item => { return item._id !== action.data._id }), action.data]
    }
}

export const saveEmpresaFailure = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isSaving: false
    }
}

export const HANDLERS = {
    [Types.GET_EMPRESAS_REQUEST]: getEmpresasRequest,
    [Types.GET_EMPRESAS_SUCCESS]: getEmpresasSuccess,
    [Types.GET_EMPRESAS_FAILURE]: getEmpresasFailure,

    [Types.EDIT_EMPRESA_REQUEST]: editEmpresaRequest,
    [Types.EDIT_EMPRESA_SUCCESS]: editEmpresaSuccess,
    [Types.EDIT_EMPRESA_FAILURE]: editEmpresaFailure,

    [Types.DEL_EMPRESA_REQUEST]: delEmpresaRequest,
    [Types.DEL_EMPRESA_SUCCESS]: delEmpresaSuccess,
    [Types.DEL_EMPRESA_FAILURE]: delEmpresaFailure,

    [Types.SAVE_EMPRESA_REQUEST]: saveEmpresaRequest,
    [Types.SAVE_EMPRESA_SUCCESS]: saveEmpresaSuccess,
    [Types.SAVE_EMPRESA_FAILURE]: saveEmpresaFailure,
}

export default createReducer(INITIAL_STATE, HANDLERS)