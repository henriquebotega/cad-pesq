import { Types } from '../actions'
import { createReducer } from 'reduxsauce';

export const INITIAL_STATE = {
    isLoading: false,
    registros: [],
    isSaving: false,
    registro: {}
}

export const getPesquisasRequest = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isLoading: true
    }
}

export const getPesquisasSuccess = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isLoading: false,
        registros: action.data
    }
}

export const getPesquisasFailure = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isLoading: false
    }
}

export const editPesquisaRequest = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isSaving: true
    }
}

export const editPesquisaSuccess = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isSaving: false,
        registro: action.data
    }
}

export const editPesquisaFailure = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isSaving: false
    }
}

export const delPesquisaRequest = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isLoading: true
    }
}

export const delPesquisaSuccess = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isLoading: false,
        registros: [...state.registros.filter(item => { return item._id !== action.id })]
    }
}

export const delPesquisaFailure = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isLoading: false
    }
}

export const savePesquisaRequest = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isSaving: true
    }
}

export const savePesquisaSuccess = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isSaving: false,
        registros: [...state.registros.filter(item => { return item._id !== action.data._id }), action.data]
    }
}

export const savePesquisaFailure = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isSaving: false
    }
}

export const HANDLERS = {
    [Types.GET_PESQUISAS_REQUEST]: getPesquisasRequest,
    [Types.GET_PESQUISAS_SUCCESS]: getPesquisasSuccess,
    [Types.GET_PESQUISAS_FAILURE]: getPesquisasFailure,

    [Types.EDIT_PESQUISA_REQUEST]: editPesquisaRequest,
    [Types.EDIT_PESQUISA_SUCCESS]: editPesquisaSuccess,
    [Types.EDIT_PESQUISA_FAILURE]: editPesquisaFailure,

    [Types.DEL_PESQUISA_REQUEST]: delPesquisaRequest,
    [Types.DEL_PESQUISA_SUCCESS]: delPesquisaSuccess,
    [Types.DEL_PESQUISA_FAILURE]: delPesquisaFailure,

    [Types.SAVE_PESQUISA_REQUEST]: savePesquisaRequest,
    [Types.SAVE_PESQUISA_SUCCESS]: savePesquisaSuccess,
    [Types.SAVE_PESQUISA_FAILURE]: savePesquisaFailure,
}

export default createReducer(INITIAL_STATE, HANDLERS)