import { Types } from '../actions'
import { createReducer } from 'reduxsauce';

export const INITIAL_STATE = {
    isLoading: false,
    registros: [],
    isSaving: false,
    registro: {}
}

export const getCategoriasRequest = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isLoading: true
    }
}

export const getCategoriasSuccess = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isLoading: false,
        registros: action.data
    }
}

export const getCategoriasFailure = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isLoading: false
    }
}

export const editCategoriaRequest = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isSaving: true
    }
}

export const editCategoriaSuccess = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isSaving: false,
        registro: action.data
    }
}

export const editCategoriaFailure = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isSaving: false
    }
}

export const delCategoriaRequest = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isLoading: true
    }
}

export const delCategoriaSuccess = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isLoading: false,
        registros: [...state.registros.filter(item => { return item._id !== action.id })]
    }
}

export const delCategoriaFailure = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isLoading: false
    }
}

export const saveCategoriaRequest = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isSaving: true
    }
}

export const saveCategoriaSuccess = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isSaving: false,
        registros: [...state.registros.filter(item => { return item._id !== action.data._id }), action.data]
    }
}

export const saveCategoriaFailure = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isSaving: false
    }
}

export const HANDLERS = {
    [Types.GET_CATEGORIAS_REQUEST]: getCategoriasRequest,
    [Types.GET_CATEGORIAS_SUCCESS]: getCategoriasSuccess,
    [Types.GET_CATEGORIAS_FAILURE]: getCategoriasFailure,

    [Types.EDIT_CATEGORIA_REQUEST]: editCategoriaRequest,
    [Types.EDIT_CATEGORIA_SUCCESS]: editCategoriaSuccess,
    [Types.EDIT_CATEGORIA_FAILURE]: editCategoriaFailure,

    [Types.DEL_CATEGORIA_REQUEST]: delCategoriaRequest,
    [Types.DEL_CATEGORIA_SUCCESS]: delCategoriaSuccess,
    [Types.DEL_CATEGORIA_FAILURE]: delCategoriaFailure,

    [Types.SAVE_CATEGORIA_REQUEST]: saveCategoriaRequest,
    [Types.SAVE_CATEGORIA_SUCCESS]: saveCategoriaSuccess,
    [Types.SAVE_CATEGORIA_FAILURE]: saveCategoriaFailure,
}

export default createReducer(INITIAL_STATE, HANDLERS)